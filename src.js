var  secuencias;
var  encabezados;
var contenidoOriginal;
var espacios = [];
var textoCifrado;
var tamañoClave;
var frecuenciaNormal = [];
var clave=[];
var frecuenciaEncriptada = [];
var divPosibleClave ;
var  divTablasFrecuencias;
var tablasFrecuenciaCalculadas= false;
var contSecuencias=0;
var tamanioClave = [];
var abecedario = [];
var matrizVigenere = new Array(27);



function cargarHTML(){
  llenarFrecuenciaNormal();
  llenarCuadroVigenere();
  divPosibleClave= document.getElementById('divPosibleClave');
  divPosibleClave.insertAdjacentHTML('afterbegin','<table id="Secuencias" ><tr id="Encabezado"><th >Secuencia Repetida</th><th >Espacio entre repeticiones</th></tr></table>');

  encabezados = document.getElementById('Encabezado');
  secuencias = document.getElementById('Secuencias');

  for(var i = 2;i<=20;i++){
    var html= '  <th style="  width: 20px;">'+i+'</th>';
    encabezados.insertAdjacentHTML('beforeend', html);
  }
}
function iniciarAnalisis(){
  textoCifrado= document.getElementById('textoCifrado').value;
  tamañoClave = getPosibleTamanioClave(4);

  divPosibleClave.insertAdjacentHTML('beforeend','<a>Posible tamanio de la clave:  </a><input type="number" value='+tamañoClave+
  ' id="tamañoClave"></input> <br><br><input id="obtenerFrecuencias" type="submit" onclick="analizarFrecuencia()" value="Obtener frecuencias"></input>');
}
function getPosibleTamanioClave(longsecuencias){
  var secuenciasBuscada= "";
  var secuenciasEncontrada= "";
  var inicioBuscada =0;
  var inicioEncontrada =0;
  var cont = 0;

  for(var i = 0;i<textoCifrado.length;i++){
    secuenciasBuscada = "";
    cont=0;
    inicioBuscada = i;
    for(var j = i; j<longsecuencias+i;j++){
     secuenciasBuscada+= textoCifrado[j];
    }
    for(var j=i;j<textoCifrado.length;j++){
      secuenciasEncontrada = "";
      inicioEncontrada = j;
      for(var k = j; k<longsecuencias+j;k++){
       secuenciasEncontrada+= textoCifrado[k];
      }
      if(secuenciasEncontrada==secuenciasBuscada){
        cont++;
        if(cont ==2){
          break;
        }
      }
    }
    if(cont>1){
      var espacio =inicioEncontrada-inicioBuscada;
      espacios.push(espacio);
      insertarValores(secuenciasBuscada,espacio );

    }
  }

  for(var i = 0 ; i<19;i++){
    for(var j = 0 ; j<espacios.length;j++){
      if(espacios[j]%(i+2)==0){
        tamanioClave[i]++;
      }
    }
  }



  return ;//getMayor(tamanioClave);

}




function insertarValores(secuenciasBuscada, espacio){


  var html= '<tr id=s'+contSecuencias+'><td>'+secuenciasBuscada+'</td><td>'+espacio+'</td></tr>';

  secuencias.insertAdjacentHTML('beforeend',html);
  var secuencia = document.getElementById('s'+contSecuencias);
  contSecuencias++;
  for(var i = 2;i<=20;i++){
    if(espacio%i==0){
        html =   '<td>X</td>';
    }else{
        html =   '<td></td>';
    }
    secuencia.insertAdjacentHTML('beforeend',html);
  }

}

function analizarFrecuencia(){

  tamañoClave = parseInt(document.getElementById('tamañoClave').value);
  clave = [];


  for(var i = 0;i<tamañoClave;i++){
    for(var k =0;k<26;k++){
      frecuenciaEncriptada[k]=0;
    }
    for(var j = i;j<textoCifrado.length;j+=tamañoClave){
      if(textoCifrado[j]=='a'){
        frecuenciaEncriptada[0]++;
      }
      if(textoCifrado[j]=='b'){
        frecuenciaEncriptada[1]++;
      }
      if(textoCifrado[j]=='c'){
        frecuenciaEncriptada[2]++;
      }
      if(textoCifrado[j]=='d'){
        frecuenciaEncriptada[3]++;
      }
      if(textoCifrado[j]=='e'){
        frecuenciaEncriptada[4]++;
      }
      if(textoCifrado[j]=='f'){
        frecuenciaEncriptada[5]++;
      }
      if(textoCifrado[j]=='g'){
        frecuenciaEncriptada[6]++;
      }
      if(textoCifrado[j]=='h'){
        frecuenciaEncriptada[7]++;
      }
      if(textoCifrado[j]=='i'){
        frecuenciaEncriptada[8]++;
      }
      if(textoCifrado[j]=='j'){
        frecuenciaEncriptada[9]++;
      }
      if(textoCifrado[j]=='k'){
        frecuenciaEncriptada[10]++;
      }
      if(textoCifrado[j]=='l'){
        frecuenciaEncriptada[11]++;
      }
      if(textoCifrado[j]=='m'){
        frecuenciaEncriptada[12]++;
      }
      if(textoCifrado[j]=='n'){
        frecuenciaEncriptada[13]++;
      }
      if(textoCifrado[j]=='o'){
        frecuenciaEncriptada[14]++;
      }
      if(textoCifrado[j]=='p'){
        frecuenciaEncriptada[15]++;
      }
      if(textoCifrado[j]=='q'){
        frecuenciaEncriptada[16]++;
      }
      if(textoCifrado[j]=='r'){
        frecuenciaEncriptada[17]++;
      }
      if(textoCifrado[j]=='s'){
        frecuenciaEncriptada[18]++;
      }
      if(textoCifrado[j]=='t'){
        frecuenciaEncriptada[19]++;
      }
      if(textoCifrado[j]=='u'){
        frecuenciaEncriptada[20]++;
      }
      if(textoCifrado[j]=='v'){
        frecuenciaEncriptada[21]++;
      }
      if(textoCifrado[j]=='w'){
        frecuenciaEncriptada[22]++;
      }
      if(textoCifrado[j]=='x'){
        frecuenciaEncriptada[23]++;
      }
      if(textoCifrado[j]=='y'){
        frecuenciaEncriptada[24]++;
      }
      if(textoCifrado[j]=='z'){
        frecuenciaEncriptada[25]++;
      }
    }

    divTablasFrecuencias  = document.getElementById('divTablasFrecuencias');
    if(tablasFrecuenciaCalculadas){
      while(divTablasFrecuencias.hasChildNodes()){
        divTablasFrecuencias.removeChild(divTablasFrecuencias.childNodes[0]);
      }
      tablasFrecuenciaCalculadas = false;
    }
var numMovimientos = compararFrecuencias(frecuenciaEncriptada);
    divTablasFrecuencias.insertAdjacentHTML('beforeend','<p>letra '+ i +'</p><table id=l'+i+'><tr><th>Letra</th><th>F Encriptada</th><th>F Normal</th><th>Numero Movimientos: '+numMovimientos+'</th></tr></table>');
    var tablasFrecuencias= document.getElementById('l'+i);

    tablasFrecuencias.insertAdjacentHTML('beforeend',
     '<tr><td>a</td><td>'+frecuenciaEncriptada[0]+'</td><td>'+frecuenciaNormal[0]+'</td></tr>'+
     '<tr><td>b</td><td>'+frecuenciaEncriptada[1]+'</td><td>'+frecuenciaNormal[1]+'</td></tr>'+
     '<tr><td>c</td><td>'+frecuenciaEncriptada[2]+'</td><td>'+frecuenciaNormal[2]+'</td></tr>'+
     '<tr><td>d</td><td>'+frecuenciaEncriptada[3]+'</td><td>'+frecuenciaNormal[3]+'</td></tr>'+
     '<tr><td>e</td><td>'+frecuenciaEncriptada[4]+'</td><td>'+frecuenciaNormal[4]+'</td></tr>'+
     '<tr><td>f</td><td>'+frecuenciaEncriptada[5]+'</td><td>'+frecuenciaNormal[5]+'</td></tr>'+
     '<tr><td>g</td><td>'+frecuenciaEncriptada[6]+'</td><td>'+frecuenciaNormal[6]+'</td></tr>'+
     '<tr><td>h</td><td>'+frecuenciaEncriptada[7]+'</td><td>'+frecuenciaNormal[7]+'</td></tr>'+
     '<tr><td>i</td><td>'+frecuenciaEncriptada[8]+'</td><td>'+frecuenciaNormal[8]+'</td></tr>'+
     '<tr><td>j</td><td>'+frecuenciaEncriptada[9]+'</td><td>'+frecuenciaNormal[9]+'</td></tr>'+
     '<tr><td>k</td><td>'+frecuenciaEncriptada[10]+'</td><td>'+frecuenciaNormal[10]+'</td></tr>'+
     '<tr><td>l</td><td>'+frecuenciaEncriptada[11]+'</td><td>'+frecuenciaNormal[11]+'</td></tr>'+
     '<tr><td>m</td><td>'+frecuenciaEncriptada[12]+'</td><td>'+frecuenciaNormal[12]+'</td></tr>'+
     '<tr><td>n</td><td>'+frecuenciaEncriptada[13]+'</td><td>'+frecuenciaNormal[13]+'</td></tr>'+
     '<tr><td>o</td><td>'+frecuenciaEncriptada[14]+'</td><td>'+frecuenciaNormal[14]+'</td></tr>'+
     '<tr><td>p</td><td>'+frecuenciaEncriptada[15]+'</td><td>'+frecuenciaNormal[15]+'</td></tr>'+
     '<tr><td>q</td><td>'+frecuenciaEncriptada[16]+'</td><td>'+frecuenciaNormal[16]+'</td></tr>'+
     '<tr><td>r</td><td>'+frecuenciaEncriptada[17]+'</td><td>'+frecuenciaNormal[17]+'</td></tr>'+
     '<tr><td>s</td><td>'+frecuenciaEncriptada[18]+'</td><td>'+frecuenciaNormal[18]+'</td></tr>'+
     '<tr><td>t</td><td>'+frecuenciaEncriptada[19]+'</td><td>'+frecuenciaNormal[19]+'</td></tr>'+
     '<tr><td>u</td><td>'+frecuenciaEncriptada[20]+'</td><td>'+frecuenciaNormal[20]+'</td></tr>'+
     '<tr><td>v</td><td>'+frecuenciaEncriptada[21]+'</td><td>'+frecuenciaNormal[21]+'</td></tr>'+
     '<tr><td>w</td><td>'+frecuenciaEncriptada[22]+'</td><td>'+frecuenciaNormal[22]+'</td></tr>'+
     '<tr><td>x</td><td>'+frecuenciaEncriptada[23]+'</td><td>'+frecuenciaNormal[23]+'</td></tr>'+
     '<tr><td>y</td><td>'+frecuenciaEncriptada[24]+'</td><td>'+frecuenciaNormal[24]+'</td></tr>'+
     '<tr><td>z</td><td>'+frecuenciaEncriptada[25]+'</td><td>'+frecuenciaNormal[25]+'</td></tr>');


     clave[i]= getLetra(numMovimientos);
  }
  tablasFrecuenciaCalculadas = true;
  var llave="";
  for(var g=0;g<clave.length;g++){
    if(clave[g]!=null){
      llave+=clave[g];
    }

  }
  divTablasFrecuencias.insertAdjacentHTML('afterbegin','<br><p> Clave: '+llave+'</p>');
  var elementoTextoCifrado = document.getElementById('textoCifrado');
  elementoTextoCifrado.insertAdjacentHTML('afterend','<p> Texto Descifrado: </p><textarea>'+descifrarMensaje(llave)+'</textarea>');
}

function compararFrecuencias(fe){
  var sumas =[];
  var suma;
  for(var i =1 ;i<frecuenciaNormal.length;i++){ // movimiento
    suma=0;
      for(var j = 0 ; j<frecuenciaNormal.length;j++){
        var ji = j+i;
          if(ji>=frecuenciaNormal.length){
            ji = ((j+i)%frecuenciaNormal.length);
          }
          suma += Math.abs(frecuenciaNormal[j]-fe[ji]);
      }
    sumas[i] = suma;
  }

  return getNumMovimientos(sumas);

}
function getNumMovimientos(array){
  var menor =array[1];
  var numMovimientos= array.length-1;
  for(var i =0; i<array.length;i++){
    if(array[i]<menor){
      menor= array[i];
      numMovimientos = i;
    }
  }
  return numMovimientos;
}
function getLetra(numMovimientos){
  if(numMovimientos==0){
    return 'a';
  }
  if(numMovimientos==1){
    return 'b';
  }
  if(numMovimientos==2){
    return 'c';
  }
  if(numMovimientos==3){
    return 'd';
  }
  if(numMovimientos==4){
    return 'e';
  }
  if(numMovimientos==5){
    return 'f';
  }
  if(numMovimientos==6){
    return 'g';
  }
  if(numMovimientos==7){
    return 'h';
  }
  if(numMovimientos==8){
    return 'i';
  }
  if(numMovimientos==9){
    return 'j';
  }
  if(numMovimientos==10){
    return 'k';
  }
  if(numMovimientos==11){
    return 'l';
  }
  if(numMovimientos==12){
    return 'm';
  }
  if(numMovimientos==13){
    return 'n';
  }
  if(numMovimientos==14){
    return 'o';
  }
  if(numMovimientos==15){
    return 'p';
  }
  if(numMovimientos==16){
    return 'q';
  }
  if(numMovimientos==17){
    return 'r';
  }
  if(numMovimientos==18){
    return 's';
  }
  if(numMovimientos==19){
    return 't';
  }
  if(numMovimientos==20){
    return 'u';
  }
  if(numMovimientos==21){
    return 'v';
  }
  if(numMovimientos==22){
    return 'w';
  }
  if(numMovimientos==23){
    return 'x';
  }
  if(numMovimientos==24){
    return 'y';
  }
  if(numMovimientos==25){
    return 'z';
  }
}

function getMayor(array){
  var mayor = array[0];
  for(var i = 0 ; i<array.length;i++){
    if(array[i]>mayor){
      mayor = array[i];
    }
  }
  return mayor;
}



function descifrarMensaje(llave){
  var tablaDescifrado = new Array(2);
  for(var i = 0 ; i<2;i++){
    tablaDescifrado[i]= new Array(textoCifrado.length);
  }
  for(var i = 0 ; i<2;i++){
    for(var j = 0 ;j<textoCifrado.length;j++){
      tablaDescifrado[i][j]= '';
    }
  }
  var result='';
  for(var i = 0 ; i <3;i++){
    for(var j = 0 ; j<textoCifrado.length;j++){
      if(i==0){
        for(var k = 0 ; k<llave.length;k++){
          tablaDescifrado[i][j+k]=llave.substr(k,1);
        }
        j+= llave.length-1;
      }
      if(i==1){
        tablaDescifrado[i][j]=textoCifrado.substr(j,1);
      }
      if(i==2){
        var caracterCifrado = tablaDescifrado[1][j];
        var caracterClave =  tablaDescifrado[0][j];
        var caracterDescifrado = descifrarCaracter(caracterCifrado,caracterClave);
        if(caracterDescifrado!=null){
            result+= caracterDescifrado;
        }

      }
    }
  }
  return result;

}
function descifrarCaracter(caracterCifrado, caracterClave){
  for(var i = 0 ; i<27;i++){
    if(matrizVigenere[i][0]== caracterClave){
      for(var j = 0 ; j<26;j++){
        if(matrizVigenere[i][j]== caracterCifrado){
              return matrizVigenere[0][j];
        }
      }
    }
  }
}
function llenarCuadroVigenere(){
    abecedario[0]='a';
    abecedario[1]='b';
    abecedario[2]='c';
    abecedario[3]='d';
    abecedario[4]='e';
    abecedario[5]='f';
    abecedario[6]='g';
    abecedario[7]='h';
    abecedario[8]='i';
    abecedario[9]='j';
    abecedario[10]='k';
    abecedario[11]='l';
    abecedario[12]='m';
    abecedario[13]='n';
    abecedario[14]='o';
    abecedario[15]='p';
    abecedario[16]='q';
    abecedario[17]='r';
    abecedario[18]='s';
    abecedario[19]='t';
    abecedario[20]='u';
    abecedario[21]='v';
    abecedario[22]='w';
    abecedario[23]='x';
    abecedario[24]='y';
    abecedario[25]='z';

  for(var i = 0 ; i <27;i++){
      matrizVigenere[i]= new Array(26);
    for(var j = 0 ; j<26;j++){
      var ji = j+i
      if(ji>=26){
        ji = (ji%26);
      }
      matrizVigenere[i][j]= abecedario[ji];
    }
  }
}



function llenarFrecuenciaNormal(){
  frecuenciaNormal[0]= 6;
  frecuenciaNormal[1]= 1;
  frecuenciaNormal[2]= 2;
  frecuenciaNormal[3]= 3;
  frecuenciaNormal[4]= 9;
  frecuenciaNormal[5]= 2;
  frecuenciaNormal[6]= 1;
  frecuenciaNormal[7]= 5;
  frecuenciaNormal[8]= 5;
  frecuenciaNormal[9]= 0;
  frecuenciaNormal[10]= 1;
  frecuenciaNormal[11]= 3;
  frecuenciaNormal[12]= 2;
  frecuenciaNormal[13]= 5;
  frecuenciaNormal[14]= 6;
  frecuenciaNormal[15]= 1;
  frecuenciaNormal[16]= 0;
  frecuenciaNormal[17]= 4;
  frecuenciaNormal[18]= 5;
  frecuenciaNormal[19]= 7;
  frecuenciaNormal[20]= 2;
  frecuenciaNormal[21]= 1;
  frecuenciaNormal[22]= 2;
  frecuenciaNormal[23]= 0;
  frecuenciaNormal[24]= 1;
  frecuenciaNormal[25]= 0;

  for(var i =0; i<19;i++){
    tamanioClave[i]=0;
  }
}
function gcdArray(input) {
  if (toString.call(input) !== "[object Array]")
        return  false;
  var len, a, b;
	len = input.length;
	if ( !len ) {
		return null;
	}
	a = input[ 0 ];
	for ( var i = 1; i < len; i++ ) {
		b = input[ i ];
		a = gcd( a, b );
	}
	return a;
}
function gcd(n1, n2){
	var r = 1;
	var l = Math.max(n1, n2);
	var s = Math.min(n1, n2);
	while (r > 0) {
		var q = Math.floor(l / s);
		var r =  l - ( q * s );
		var gcd = s;
		var l = s;
		var s = r;
	}
	return gcd;
}
